import { Component, OnInit } from '@angular/core';
import {Observable } from 'rxjs';
import { DataServiceService } from '../dataService/data-service.service';
import { Team } from '../models/team';

@Component({
  selector: 'app-view-league-table',
  templateUrl: './view-league-table.component.html',
  styleUrls: ['./view-league-table.component.css']
})
export class ViewLeagueTableComponent implements OnInit {

  teams: Team[];

  constructor(private dataService: DataServiceService) { }

  ngOnInit() 
  {
    this.getAFLTeams();
  }

  getAFLTeams():void{
    this.dataService.getTeams().subscribe(temp=>{this.teams=temp;});
  }

}
