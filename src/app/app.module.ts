import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ViewLeagueTableComponent } from './view-league-table/view-league-table.component';
import { DataServiceService } from './dataService/data-service.service';
import { ViewTeamGamesComponent } from './view-team-games/view-team-games.component';
import { GameServiceService } from './gameService/game-service.service';
import { LadderTableComponent } from './ladder-table/ladder-table.component';
import { LadderServiceService } from './ladderService/ladder-service.service';
import { SourcesComponentComponent } from './sources-component/sources-component.component';
import { SourcesServiceService } from './sourcesService/sources-service.service';
import { TipsComponentComponent } from './tips-component/tips-component.component';
import { TipServiceService } from './tipService/Tip-Service.Service';

@NgModule({
  declarations: [
    AppComponent,
    ViewLeagueTableComponent,
    ViewTeamGamesComponent,
    LadderTableComponent,
    SourcesComponentComponent,
    TipsComponentComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [DataServiceService,GameServiceService,LadderServiceService,
  SourcesServiceService,TipServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
