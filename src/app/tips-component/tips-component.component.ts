import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import { TipServiceService} from '../tipService/tip-service.service';
import { Tips } from '../models/Tips';

@Component({
  selector: 'app-tips-component',
  templateUrl: './tips-component.component.html',
  styleUrls: ['./tips-component.component.css']
})
export class TipsComponentComponent implements OnInit {

  tips: Tips[];

  constructor(private tipService:TipServiceService) { }

  ngOnInit() {
    this.getAFLTips();
  }

  getAFLTips():void{
    this.tipService.getTips().subscribe(temp=>{
      this.tips=temp;
    })
  }

}
