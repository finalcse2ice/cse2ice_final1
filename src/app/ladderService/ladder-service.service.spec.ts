import { TestBed } from '@angular/core/testing';

import { LadderServiceService } from './ladder-service.service';

describe('LadderServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LadderServiceService = TestBed.get(LadderServiceService);
    expect(service).toBeTruthy();
  });
});
