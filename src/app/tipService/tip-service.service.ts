import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import { Tips } from '../models/tips';

@Injectable({
  providedIn: 'root'
})
export class TipServiceService {

  constructor(private http:HttpClient) { }

  ngOnInit()
  {
    this.getTips()
  }

  getTips():Observable<Tips[]>
  {
    return this.http.get('https://api.squiggle.com.au/?q=tips').pipe(
      map((data:any)=>data.tips.map((item5:any)=>new Tips(

        item5.sourceid,
        item5.tip,
        item5.gameid,
        item5.tipteamid,
        item5.err,
        item5.year,
        item5.ateam,
        item5.margin,
        item5.hteam,
        item5.updated,
        item5.hteamid,
        item5.round,
        item5.ateamid,
        item5.correct,
        item5.source,
        item5.venue,
        item5.date,
        item5.confidence,
        item5.hconfidence,
        item5.bits
        
      ))
    ))
  }
}
