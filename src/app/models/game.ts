export class Game{

    constructor
    (

        public abehinds:number,
        public hscore:number,
        public ascore: number,
        public winner:string,
        public id:number,
        public round:number,
        public agoals:number,
        public date:string,
        public year:string,
        public is_final:number,
        public venue:string,
        public winnerteamid:number,
        public updated:string,
        public hgoals:number,
        public ateamid:number,
        public ateam:string,
        public complete:number,
        public hteamid:number,
        public tz:string,
        public hbehinds:number,
        public hteam:string

    ){}
    }