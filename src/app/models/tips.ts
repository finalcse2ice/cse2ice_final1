export class Tips
{
    constructor(
        public sourceid:number,
        public tip:string,
        public gameid:number,
        public tipteamid:number,
        public err:string,
        public year:string,
        public ateam:string,
        public margin:string,
        public hteam:string,
        public updated:string,
        public hteamid:number,
        public round:number,
        public ateamid:number,
        public correct:number,
        public source:string,
        public venue:string,
        public date:string,
        public confidence:string,
        public hconfidence:string,
        public bits:string

    ){}
}