export class Source
{

    constructor
    (
        public url:string,
        public id:number,
        public name:string
    )
    {}

}