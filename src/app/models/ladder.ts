export class Ladder{

    constructor(
     public round :number,
     public team: string,
     public year:number,
     public teamid:number,
     public sourceid:number,
     public updated:string,
     public wins:string,
     public mean_rank:string,
     public rank:number,
     public source:string,
     public percentage:string,
     public swarms:string   
        
        
    ){}
}