import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {SourcesServiceService } from '../sourcesService/sources-service.service';
import { Source } from '../models/sources';

@Component({
  selector: 'app-sources-component',
  templateUrl: './sources-component.component.html',
  styleUrls: ['./sources-component.component.css']
})
export class SourcesComponentComponent implements OnInit {

  sources: Source[];

  constructor(private sourceService: SourcesServiceService) { }

  ngOnInit()
  {
    this.getAFLSources();
  }

  getAFLSources():void{
    this.sourceService.getSources().subscribe(
      temp=>{this.sources=temp;}
    )
  }

}
