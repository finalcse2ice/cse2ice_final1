import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SourcesComponentComponent } from './sources-component.component';

describe('SourcesComponentComponent', () => {
  let component: SourcesComponentComponent;
  let fixture: ComponentFixture<SourcesComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SourcesComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SourcesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
