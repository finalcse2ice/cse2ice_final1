import { Component, OnInit } from '@angular/core';
import {Observable} from  'rxjs';
import { GameServiceService } from '../gameService/game-service.service';
import { Game } from '../models/game';

@Component({
  selector: 'app-view-team-games',
  templateUrl: './view-team-games.component.html',
  styleUrls: ['./view-team-games.component.css']
})
export class ViewTeamGamesComponent implements OnInit {

  games: Game[];

  constructor(private gameService:GameServiceService) { }

  ngOnInit() {
    this.getAFLGames();
  }

  getAFLGames():void{
    this.gameService.getGames().subscribe(
      temp=>{
        this.games=temp;}
      
    )
  }

}
