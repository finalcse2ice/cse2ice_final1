import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators';
import {HttpClient} from '@angular/common/http';
import { Game} from '.././models/game';

@Injectable({
  providedIn: 'root'
})
export class GameServiceService {

  constructor(private http:HttpClient) { }

  ngOnInit()
  {
    this.getGames();
  }

  getGames():Observable<Game[]>
  {
    return this.http.get('https://api.squiggle.com.au/?q=games').pipe
    (map((data:any)=>data.games.map((item2:any)=>new Game(
      item2.abehinds,
      item2.hscore,
      item2.ascore,
      item2.winner,
      item2.id,
      item2.round,
      item2.agoals,
      item2.date,
      item2.year,
      item2.is_final,
      item2.venue,
      item2.winnerteamid,
      item2.updated,
      item2.hgoals,
      item2.ateamid,
      item2.ateam,
      item2.complete,
      item2.hteamid,
      item2.tz,
      item2.hbehinds,
      item2.hteam

    ))
     ))
  }
}
